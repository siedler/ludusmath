﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Personalizacao;
using System;

public class comandosBasicos : MonoBehaviour {
    private Animator controleTela;

	public Animator anim;
    
    public void carregaCena(string nomeCena)
    {
        SceneManager.LoadScene(nomeCena);
    }

	public void carregaHub()
	{
		if (!PlayerPrefs.HasKey ("skin")) {
			PlayerPrefs.SetString ("skin", "fazenda");
		}
		//se não definiu a orientação da pergunta seta horizontal
		if (!PlayerPrefs.HasKey ("orientacao")) {
			PlayerPrefs.SetInt ("orientacao", 0);
		}
	}

    public void resetJogo()
    {
        PlayerPrefs.DeleteAll();
    }

    public void telaParabens()
    {
        anim.SetBool("mudar", true);
    }

    public void abrirSite()
    {
        Application.OpenURL("https://ludus.programatche.net/");
    }

	    public void Animation()
    {
        controleTela = GetComponent<Animator>();
        float time;
        
            time = 5;
        

        
            if (time < 0)
            {
            controleTela.SetBool("Normal", time == 0);
        }
            else
            {
                time -= Time.deltaTime;
            }

    }

    public void EncerrarAtividade()
    {
        Application.Quit();
    }

    public void AbrirArquivo()
    {
        Application.OpenURL("C:/Users/Junior/Documents/GitHub/-ludus/Assets/Resources/Outros/Manual_do_Usuario.pdf");
    }

    public void AbrirhubFazenda()
    {
        if (!PlayerPrefs.HasKey("skin"))
        {
            PlayerPrefs.SetString("skin", "Fazenda");
        }

        Skin.Carregar();
        SceneManager.LoadScene("hubFases"+ PlayerPrefs.GetString("skin"));
    }

    public void AbrirhubEgito()
    {
        PlayerPrefs.SetString("skin", "Egito");
        Skin.Carregar();
        SceneManager.LoadScene("hubFasesEgito");
    }

    public void AbrirhubFadas()
    {
        PlayerPrefs.SetString("skin", "Fadas");
        Skin.Carregar();
        SceneManager.LoadScene("hubFasesFadas");
    }

    public void AbrirhubSereias()
    {
        PlayerPrefs.SetString("skin", "Sereias");
        Skin.Carregar();
        SceneManager.LoadScene("hubFasesSereias");
    }

    public void AbrirhubOrcs()
    {
        PlayerPrefs.SetString("skin", "Orcs");
        Skin.Carregar();
        SceneManager.LoadScene("hubFasesOrcs");
    }

    ////aqui vê qual hub abrir, caso o usuário tenha comprado na loja.
    //String EstiloJogo;
    //EstiloJogo = "Scenes/" + "hubFases/" + "hubFases" + PlayerPrefs.GetString("skin");

    //    Skin.Carregar();
    //    SceneManager.LoadScene(EstiloJogo

    public void AvancarParabens() {

        String skin = "hubFases" + PlayerPrefs.GetString("skin");

        Debug.Log(skin);
       
        SceneManager.LoadScene(skin);

        //SceneManager.LoadScene("hubFases" + PlayerPrefs.GetString("skin"));
        
    }
}
