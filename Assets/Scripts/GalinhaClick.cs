﻿using UnityEngine;
using UnityEngine.EventSystems;

public class GalinhaClick : MonoBehaviour, IPointerClickHandler
{ 
	public string alternativa;
	public void OnPointerClick(PointerEventData eventData)
	{
		Pergunta p = new Pergunta ();
		p.verificaResposta (alternativa);
	}
}
